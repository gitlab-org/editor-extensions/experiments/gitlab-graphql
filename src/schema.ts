export const typeDefs = `#graphql
scalar ID

type CurrentUser {
  id: ID!

  # Fields introduced in 16.8+
  duoChatAvailable: Boolean!            @deprecated(reason: "**Status**: Experiment. Introduced in 16.8.")
  duoCodeSuggestionsAvailable: Boolean! @deprecated(reason: "**Status**: Experiment. Introduced in 16.8.")
}

type Project {
  fullPath: ID!

  # Fields introduced in 16.9+
  duoFeaturesEnabled: Boolean @deprecated(reason: "**Status**: Experiment. Introduced in 16.9.")
}

type Query {
  currentUser: CurrentUser
  project(fullPath: ID!): Project
}
`;
