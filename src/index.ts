// vi: set fdm=marker :
import casual from 'casual';
import { ApolloServer } from '@apollo/server';
import { addMocksToSchema } from '@graphql-tools/mock';
import { defaultFieldResolver, GraphQLSchema } from 'graphql'
import { getDirective, MapperKind, mapSchema } from '@graphql-tools/utils';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { startStandaloneServer } from '@apollo/server/standalone';
import { typeDefs } from './schema.js';

const MILESTONE_AT_BUILD = '16.9';

// {{{[transformDeprecatedSchema(schema: GraphQLSchema) => GraphQLSchema]
const transformDeprecatedSchema = (schema: GraphQLSchema) => mapSchema(schema, {
  [MapperKind.OBJECT_FIELD]: (fieldConfig) => {
    const deprecation = getDirective(schema, fieldConfig, 'deprecated')?.[0]
    if (deprecation) {
      const { resolve = defaultFieldResolver } = fieldConfig
      return {
        ...fieldConfig,
        resolve: async function (source, args, context, info) {
          console.error(`context.milestone: ${JSON.stringify(context.milestone)}`)
          console.error(`deprecation: ${JSON.stringify(deprecation)}`)
          if (context.milestone == "16.5") {
            throw new Error(`Field '${(fieldConfig as unknown)?.name}' doesn't exist on type 'Project'`)
          }
          return resolve(source, args, context, info)
        }
      }
    }
  },
});
// }}}

//{{{[mocks = {...}]
const mocks = {
  ID: () => casual.uuid,
  Int: () => casual.integer(0, 999),
  Float: () => 6.28,
  String: () => casual.name,
};
//}}}

interface GitLabContext {
  milestone?: String;
}

const server = new ApolloServer<GitLabContext>({
  schema: transformDeprecatedSchema(addMocksToSchema({
    schema: makeExecutableSchema({ typeDefs, resolvers: {} }),
    mocks,
  })),
});

const { url } = await startStandaloneServer(server, {
  listen: { port: 4000 },
  context: async ({ req, res }) => {
//    console.warn(`req.headers: ${JSON.stringify(req.headers)}`)
    return ({
      milestone: req.headers['gitlab-graphql-milestone'] || MILESTONE_AT_BUILD
    })
  },
});

console.log(`🚀 Server listening at: ${url}`);
console.log(`⚙️  Explorer available at: ${url}graphiql`);
