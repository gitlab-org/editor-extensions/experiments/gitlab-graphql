FROM node:current-alpine

WORKDIR /gitlab-graphql

ADD package.json .
ADD package-lock.json .
ADD dist/index.js .
ADD dist/schema.js .

RUN npm install --omit=dev

EXPOSE 4000/tcp

CMD ["index.js"]
