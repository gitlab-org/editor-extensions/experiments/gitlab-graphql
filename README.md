# gitlab-graphql

An Apollo Server which simulates running different GitLab distributions and license tiers.

## Usage

By default the `gitlab-graphql` project 

```shell
docker run -it -e GITLAB_GRAPHQL_REMOVE_DEPRECATED=true \
               -e GITLAB_GRAPHQL_TOKEN_SCOPES='["ai_features", "read_api"]' \
               registry.gitlab.com/gitlab-org/editor-extensions/contracts/gitlab-graphql:latest 
```

### Configuration

Any of the below keys can be set as environment variables in the format `"GITLAB_GRAPHQL_" + key.upcase`.

| Key                 | Value     | Default | Description |
|---------------------|-----------|---------|-------------|
| `remove_deprecated` | `boolean` | `false` | Whether to hide `@deprecated` (deprecated or experimental) fields. |
